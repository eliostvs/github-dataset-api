package com.hackerrank.github.core.domain;

import lombok.Value;

@Value
public class Identity {
    private final Long number;
}
