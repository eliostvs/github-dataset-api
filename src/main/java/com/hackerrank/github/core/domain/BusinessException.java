package com.hackerrank.github.core.domain;

public class BusinessException extends DomainException {
    public BusinessException(String message) {
        super(message);
    }
}
